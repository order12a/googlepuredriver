package com.google;

import com.google.bases.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.WebDriverWait;

import static helpers.CustomConditions.sizeOf;
import static org.openqa.selenium.support.ui.ExpectedConditions.textToBePresentInElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.urlToBe;

public class GoogleSearchTest extends BaseTest{
    public WebDriverWait wait;

    @Before
    public void setUp(){
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void testSearchAndFollowLink(){
        driver.get("http://google.com/ncr");
        search("Selenium automates browsers");

        assertSearchResultNumberEqual(10);
        wait.until(textToBePresentInElementLocated(searchResults, "Selenium automates browsers"));

        followLink(0);

        wait.until(urlToBe("http://www.seleniumhq.org/"));
    }

    By searchResults = By.cssSelector(".srg>.g");

    public void search(String phrase){
        driver.findElement(By.name("q")).sendKeys(phrase, Keys.ENTER);
    }

    public void assertSearchResultNumberEqual(int number){
        wait.until(sizeOf(searchResults, number));
    }

    public void followLink(int index){
        driver.findElements(searchResults).get(index).findElement(By.cssSelector("h3>a")).click();
    }
}
